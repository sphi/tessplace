# tess.place
A location sharing frontend for tracking Tess and other people. This code is deployed to [tess.place](https://tess.place/).

## Tech
The backend is Python using the Sanic web framework. The frontend is pure HTML, CSS and js and uses the Leaflet OpenStreetMap library for maps.

## Set up a dev env
With python3 installed, run this in the project root directory:
```
python -m venv venv && venv/bin/pip install -r backend/requirements.txt
```
You can now run this to typecheck the backend and run it locally:
```
venv/bin/python backend/server.py --dev
```
At the top of the sanic box will be a local IP:port, go to this in your browser to test. You can simulate a location update by sending a POST request with `curl` to the GPS logger URL.

## Deploying
Deploying should be much the same as setting up a dev environment, except that you leave off the `--dev` and put something like nginx between the backend and the internet (for logging/SSL support/etc). [here](backend/nginx.conf) is the nginx config I use.
