#!/usr/bin/env python3

import sys
import os
import time

from sanic import Sanic, Request, response, text, empty, json

import repo
from model import Model, Account, Location

app = Sanic('tessplace')

frontend_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'frontend')
index_path = os.path.join(frontend_dir, 'index.html')
app.static("/", index_path, name='index')
app.static("/", frontend_dir, name='files')

@app.get('/view/<token:str>')
async def view_location(request: Request, token: str):
    return await response.file(index_path)

@app.get('/locations')
async def get_locations(request: Request):
    account_set: set[Account] = set()
    locations = get_model().public_locations(account_set)
    invalid_tokens = []
    for token in request.args.getlist('t', []):
        share = get_model().share_from(token)
        if share is None:
            invalid_tokens.append(token)
        elif share.account not in account_set:
            data = share.location_request(token)
            if data is not None:
                account_set.add(share.account)
                locations.append(data)
    return json({'locations': locations, 'invalid_tokens': invalid_tokens})

@app.get('/tess_location')
async def tess_location(request: Request):
    return json(get_model().tess_location())

@app.post('/create_account')
async def create_account(request: Request):
    name = request.args.get('name')
    token = get_model().create_account(name)
    return response.text(token)

@app.post('/log/<token:str>')
async def log_location(request: Request, token: str):
    lat = float(request.args.get('lat'))
    lon = float(request.args.get('lon'))
    acc_str = request.args.get('acc', None)
    acc = float(acc_str) if acc_str is not None else None
    location = Location(lat, lon, acc, time.time())
    get_model().log(token, location)
    return response.json({'result': 'ok'}) # This is just for Overland compat

@app.get('/edit/<token:str>')
async def account_page(request: Request, token: str):
    if get_model().has_account(token):
        return await response.file(os.path.join(frontend_dir, 'edit.html'))
    else:
        return await response.file(os.path.join(frontend_dir, 'invalid-token.html'))

@app.get('/info/<token:str>')
async def account_info(request: Request, token: str):
    return response.json(get_model().account_info(token))

@app.post('/save/<token:str>')
async def account_update(request: Request, token: str):
    get_model().update_account(token, request.json)
    return empty()

@app.post('/delete/<token:str>')
async def delete_account(request: Request, token: str):
    get_model().delete_account(token)
    return empty()

@app.post('/create_share/<account_token:str>')
async def share_create(request: Request, account_token: str):
    return text(get_model().create_share(account_token))

@app.post('/save_share/<account_token:str>/<share_token:str>')
async def share_update(request: Request, account_token: str, share_token: str):
    get_model().update_share(account_token, share_token, request.json)
    return empty()

@app.post('/delete_share/<account_token:str>/<share_token:str>')
async def share_delete(request: Request, account_token: str, share_token: str):
    get_model().delete_share(account_token, share_token)
    return empty()

repo.init(app);

def get_model() -> Model:
    return Sanic.get_app().ctx.model

@app.listener("before_server_start")
async def before_start(app):
    app.ctx.model = Model()
    app.add_task(app.ctx.model.background_task(), name='background_task')

@app.listener("before_server_stop")
async def before_stop(app):
    await app.cancel_task('background_task')

@app.listener("after_server_stop")
def after_stop(app):
    get_model().server_stopped()

if __name__ == '__main__':
    # Support both to match the warning sanic gives automatically when run in production mode
    debug = '--debug' in sys.argv or '--dev' in sys.argv
    app.run(
        port=5075,
        single_process=True,
        access_log=debug,
        debug=debug
    )
