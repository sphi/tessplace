import asyncio
import pathlib
import logging
from typing import Optional

from sanic import Sanic, Request, text, empty

cached_latest_commit: Optional[str] = None

def get_repo_dir() -> str:
    return str(pathlib.Path(__file__).resolve().parent.parent)

async def get_latest_commit_internal() -> Optional[str]:
    process = await asyncio.create_subprocess_exec(
        'git', '-C', get_repo_dir(), 'rev-parse', 'HEAD',
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    stdout, stderr = await process.communicate()
    if process.returncode != 0:
        logging.error(
            'getting latest commit hash exited with code '
            f'{process.returncode}: {stderr.decode().strip()}')
        return None
    return stdout.decode().strip()

async def get_latest_commit() -> Optional[str]:
    try:
        return await asyncio.wait_for(get_latest_commit_internal(), timeout=1)
    except asyncio.exceptions.TimeoutError:
        logging.error('getting latest commit hash timed out')
        return None

async def run_git_pull() -> None:
    process = await asyncio.create_subprocess_exec(
        'git', '-C', get_repo_dir(), 'pull',
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    stdout, stderr = await process.communicate()
    if process.returncode != 0:
        logging.error(
            'git pull exited with code '
            f'{process.returncode}: {stderr.decode().strip()}')

def init(app: Sanic) -> None:
    @app.route('/trigger_update', methods=["POST"])
    async def trigger_update(request: Request):
        logging.info('pulling git')
        await run_git_pull()
        commit_after = await get_latest_commit()
        # Detect if there are no updates, probably because this was sent by someone other than codeberg
        if commit_after == cached_latest_commit:
            logging.warning("we have been bamboozled. there are no updates available. continuing")
            return text('no updates available', status=400)
        else:
            app.stop()
            return empty()

    @app.route('/last_commit', methods=["GET"])
    def latest_commit(request: Request):
        if cached_latest_commit is not None:
            return text(cached_latest_commit)
        else:
            return text(str('last commit hash unknown'), status=500)

    @app.listener("before_server_start")
    async def before_start(app):
        global cached_latest_commit
        cached_latest_commit = await get_latest_commit()
