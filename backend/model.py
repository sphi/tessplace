import time
import asyncio
import json
import random
import string
import logging
import threading
from typing import Optional

import google_maps

def create_token() -> str:
    characters = string.ascii_letters + string.digits
    return '-'.join(''.join(random.choice(characters) for i in range(5)) for j in range(4))

class Location():
    def __init__(self, latitude: float, longitude: float, accuracy: Optional[float], time: float):
        self.latitude = latitude
        self.longitude = longitude
        self.accuracy = accuracy
        self.time = time

class Share:
    def __init__(self, token: str, account: 'Account', label: str, active: bool, timeout: Optional[float]):
        self.token = token
        self.account = account
        self.label = label
        self.active = active
        self.timeout = timeout

    def serialize(self) -> dict:
        return {
            'token': self.token,
            'label': self.label,
            'active': self.active,
            'timeout': self.timeout,
        }

    @staticmethod
    def deserialize(account: 'Account', data: dict) -> 'Share':
        token = data.pop('token')
        assert isinstance(token, str)
        label = data.pop('label')
        assert isinstance(label, str)
        timeout = data.pop('timeout', False)
        assert isinstance(timeout, float) or isinstance(timeout, int) or timeout is None
        active = data.pop('active')
        assert isinstance(active, bool)
        return Share(token, account, label, active, timeout)

    def make_inactive_if_timeout_past(self) -> None:
        if self.timeout is not None and self.timeout < time.time():
            self.active = False
            self.timeout = None

    def location_request(self, token: str) -> Optional[dict]:
        self.account.clear_location_if_old()
        self.account.last_location_request = time.time()
        self.make_inactive_if_timeout_past()
        loc = self.account.location
        if self.active and loc is not None:
            return {
                'name': self.account.title,
                'token': token,
                'lat': loc.latitude,
                'lon': loc.longitude,
                'acc': loc.accuracy,
                'time': loc.time
            }
        else:
            return None

class Account:
    def __init__(self, token: str, public_id: str, title: str, google_share: str, public: bool) -> None:
        self.token = token
        self.public_id = public_id
        self.title = title
        self.google_share = google_share
        self.last_google_update = 0.0
        self.consecutive_google_failures = 0
        self.location: Optional[Location] = None
        # Last time that this account got a location or was updated
        self.active_time = time.time()
        self.last_location_request = self.active_time
        self.shares: dict[str, Share] = {}
        self.public = Share('', self, '[public]', public, None)
        self.private = Share('', self, '[private]', True, None)

    def clear_location_if_old(self) -> None:
        if self.location is not None:
            if time.time() - self.location.time > 60 * 60 * 24:
                self.location = None

    def update_google_locaiton(self) -> None:
        current_time = time.time()
        time_since_last_location_request = current_time - self.last_location_request
        # Update every 30 seconds if locaiton has been queried in last five minutes, else update every 15 minutes
        time_to_update = 30 if time_since_last_location_request < 300 else 900
        if self.google_share and current_time - self.last_google_update > time_to_update:
            self.last_google_update = current_time
            try:
                lat, lon = google_maps.location_from_share_link(self.google_share)
                self.location = Location(lat, lon, None, current_time)
                self.active_time = current_time
                self.consecutive_google_failures = 0
            except Exception as e:
                logging.error('getting location from google maps: ' + str(e))
                logging.error('google share is ' + str(self.google_share))
                self.consecutive_google_failures += 1
                if self.consecutive_google_failures > 5:
                    logging.error('google share failed too many times, likely expired, removing')
                    self.google_share = ''

    def serialize(self) -> dict:
        return {
            'token': self.token,
            'public_id': self.public_id,
            'title': self.title,
            'google_share': self.google_share,
            'public': self.public.active,
            'shares': [share.serialize() for share in self.shares.values()],
        }

    @staticmethod
    def deserialize(data: dict) -> 'Account':
        token = data.pop('token')
        assert isinstance(token, str)
        public_id = data.pop('public_id', None)
        if public_id is None:
            public_id = create_token()
        assert isinstance(public_id, str)
        title = data.pop('title')
        assert isinstance(title, str)
        google_share = data.pop('google_share', '')
        assert isinstance(google_share, str)
        public = data.pop('public', False)
        assert isinstance(public, bool)
        account = Account(token, public_id, title, google_share, public)
        shares = data.pop('shares', [])
        assert isinstance(shares, list)
        for share_data in shares:
            share = Share.deserialize(account, share_data)
            account.shares[share.token] = share
        assert not data, 'data members left over after deserialization ' + repr(data)
        return account

class Model:
    def __init__(self) -> None:
        self.accounts: dict[str, Account] = {}
        self.shares: dict[str, Share] = {}
        self.tess_token = ''
        self.needs_save = False
        self.stopped = False
        self.accounts_with_google_share: set[Account] = set()
        self.google_thread = threading.Thread(target=self.update_google_locaitons)
        self.google_thread.start()
        try:
            with open('db.json', 'r') as f:
                data = json.load(f)
            accounts = data.pop('accounts')
            self.tess_token = data.pop('tess_token', '')
            assert not data, 'loaded data has invalid properties: ' + repr(list(data.keys()))
            for account_data in accounts:
                self.add_account(Account.deserialize(account_data))

        except FileNotFoundError:
            pass

    def add_account(self, account: Account) -> None:
        self.accounts[account.token] = account
        for share in account.shares.values():
            self.shares[share.token] = share
        if account.google_share:
            self.accounts_with_google_share.add(account)

    def create_account(self, name: str) -> str:
        token = create_token()
        account = Account(token, create_token(), name, '', False)
        self.add_account(account)
        return token

    def log(self, token: str, location: Location) -> None:
        account = self.accounts.get(token)
        assert account, 'invalid account token'
        account.location = location
        account.active_time = max(account.active_time, location.time)

    def delete_account(self, token: str) -> None:
        account = self.accounts.pop(token)
        assert account, 'invalid account token'
        for share in account.shares.values():
            self.shares.pop(share.token)
        self.accounts_with_google_share.discard(account)
        self.save()

    def create_share(self, account_token: str) -> str:
        account = self.accounts.get(account_token)
        assert account, 'invalid account token'
        token = create_token()
        share = Share(token, account, '', True, None)
        account.shares[token] = share
        self.shares[token] = share
        return token

    def delete_share(self, account_token: str, share_token: str) -> None:
        account = self.accounts.get(account_token)
        assert account, 'invalid account token'
        share = account.shares.pop(share_token)
        assert share, 'invalid share token'
        self.shares.pop(share_token)
        share.timeout = 0 # should not be needed
        self.save()

    def update_share(self, account_token: str, share_token: str, info: dict):
        account = self.accounts.get(account_token)
        assert account, 'invalid account token'
        share = account.shares.get(share_token)
        assert share, 'invalid share token'
        if 'label' in info:
            assert isinstance(info['label'], str)
            share.label = info.pop('label')
        if 'active' in info:
            assert isinstance(info['active'], bool)
            share.active = info.pop('active')
        if 'timeout' in info:
            assert isinstance(info['timeout'], float) or isinstance(info['timeout'], int) or info['timeout'] is None
            share.timeout = info.pop('timeout')
        self.save()
        assert not info, 'share update info has invalid properties: ' + repr(list(info.keys()))

    def public_locations(self, used_accounts: set[Account]) -> list[dict]:
        result: list[dict] = []
        for account in self.accounts.values():
            data = account.public.location_request(account.public_id)
            if data is not None:
                used_accounts.add(account)
                if account.token == self.tess_token:
                    data['is_tess'] = True
                result.append(data)
        return result

    def share_from(self, token: str) -> Optional[Share]:
        share = self.shares.get(token)
        if share is None and token in self.accounts:
            share = self.accounts[token].private
        return share

    def tess_location(self) -> dict:
        account = self.accounts.get(self.tess_token)
        assert account, 'no known tess account'
        data = account.public.location_request(account.public_id)
        assert data, 'no recent location for tess'
        return data

    def account_info(self, token: str) -> dict:
        account = self.accounts.get(token)
        assert account, 'invalid account token'
        account.clear_location_if_old()
        account.last_location_request = time.time()
        return {
            'name': account.title,
            'google_share_link': account.google_share,
            'location': account.private.location_request(token),
            'public': account.public.active,
            'shares': [{
                'token': share.token,
                'label': share.label,
                'active': share.active,
                'timeout': share.timeout,
            } for share in account.shares.values()]
        }

    def has_account(self, token: str) -> bool:
        return token in self.accounts

    def update_account(self, token: str, info: dict):
        account = self.accounts.get(token)
        assert account, 'invalid account token'
        account.active_time = time.time()
        if 'name' in info:
            assert isinstance(info['name'], str)
            account.title = info.pop('name')
        if 'google_share_link' in info:
            assert isinstance(info['google_share_link'], str)
            account.google_share = info.pop('google_share_link')
            if account.google_share:
                self.accounts_with_google_share.add(account)
            else:
                self.accounts_with_google_share.discard(account)
        if 'public' in info:
            assert isinstance(info['public'], bool)
            account.public.active = info.pop('public')
        self.save()
        assert not info, 'account update info has invalid properties: ' + repr(list(info.keys()))

    def clean_up(self) -> None:
        current_time = time.time()
        accounts_to_remove = []
        google_shares: set[str] = set()
        for account in reversed(self.accounts.values()):
            account.clear_location_if_old()
            if account.token != self.tess_token and current_time - account.active_time > 365 * 24 * 60 * 60:
                accounts_to_remove.append(account)
            elif not account.title and current_time - account.active_time > 30 * 24 * 60 * 60:
                accounts_to_remove.append(account)
            elif account.google_share and account.google_share in google_shares:
                accounts_to_remove.append(account)
            google_shares.add(account.google_share)
            for share in account.shares.values():
                share.make_inactive_if_timeout_past()
        for account in accounts_to_remove:
            self.delete_account(account.token)
        if accounts_to_remove:
            self.needs_save = True

    def update_google_locaitons(self) -> None:
        while not self.stopped:
            accounts = list(self.accounts_with_google_share)
            for account in accounts:
                account.update_google_locaiton()
                if not account.google_share:
                    self.accounts_with_google_share.discard(account)
                    self.save()
                if self.stopped:
                    return
            for i in range(10):
                time.sleep(0.5)
                if self.stopped:
                    return

    async def background_task(self) -> None:
        while True:
            self.clean_up()
            if self.needs_save:
                self.save()
            await asyncio.sleep(300)

    def save(self) -> None:
        data = {
            'tess_token': self.tess_token,
            'accounts': [Account.serialize(item) for item in self.accounts.values()],
        }
        with open('db.json', 'w') as f:
            json.dump(data, f)
        self.needs_save = False

    def server_stopped(self) -> None:
        self.save()
        self.stopped = True
        self.google_thread.join()
