import re
import requests # type: ignore
import json

cached_links: dict[str, str] = {}

def data_param_from_share_link(share_link: str) -> str:
    global cached_links
    assert share_link.startswith('https://maps.app.goo.gl/'), 'invalid share link'
    if share_link not in cached_links:
        resp = requests.get(share_link, allow_redirects=True)
        maps_link = resp.url
        cached_links[share_link] = re.findall(r'/data=(.*?)(?:\?|$)', maps_link)[0]
    return cached_links[share_link]

def location_from_share_link(share_link: str) -> tuple[float, float]:
    data_parameter = data_param_from_share_link(share_link)

    things = data_parameter.split("!")
    thing1 = things[4]
    thing2 = things[5]

    share_rpc_url = f"https://www.google.com/maps/rpc/locationsharing/read?authuser=0&hl=en&gl=us&pb=!1e1!2m2!1sWb6HZKSDKMSq5NoPzdyXoAQ!7e81!3m2!{thing1}!{thing2}"
    share_rpc_text = requests.get(share_rpc_url).text
    share_rpc = json.loads(share_rpc_text.split("\n", 1)[1])

    data = share_rpc[0][0]
    assert data[1], 'no data[1]'
    # https://github.com/Vaelek/node-google-shared-locations/blob/master/index.js#L742
    return (float(data[1][1][2]), float(data[1][1][1]))
