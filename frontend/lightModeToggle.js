var isLightMode = localStorage.getItem('light-mode') == 'true';
document.body.classList.toggle('light', isLightMode);
function onLightModeToggled() {
  isLightMode = !isLightMode;
  localStorage.setItem('light-mode', isLightMode);
  document.body.classList.toggle('light', isLightMode);
}
