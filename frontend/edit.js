const locationElem = document.getElementById('location');
const statusElem = document.getElementById('status');
const nameElem = document.getElementById('name-field');
const shareLinksFilterElem = document.getElementById('share-link-filter');
const shareLinksDivElem = document.getElementById('share-link-list-div');
const shareLinkTemplate = document.getElementById('share-link-template');
const googleShareError = document.getElementById('google-share-error');
const googleShareElem = document.getElementById('google-share-field');
const loggerUrlElem = document.getElementById('gps-logger-url');
const editSharePopupContainer = document.getElementById('edit-share-popup-container');
const deleteConfirmPopupContainer = document.getElementById('delete-confirm-popup-container');
const editShareLabelElem = document.getElementById('edit-share-label-field');
const editShareTimeoutElem = document.getElementById('edit-share-timeout-field');

var currentEditingShare = null;
var editShareIsActive = false;
var sharePublic = false;
const token = window.location.pathname.split('/')[2];
localStorage.setItem('my-token', token);
loggerUrlElem.value = window.location.origin + '/log/' + token + '?lat=%LAT&lon=%LON&acc=%ACC';

function shareLinkForToken(token) {
  return window.location.origin + '/view/' + token;
}

function applyShareLinkFilter() {
  const filterText = shareLinksFilterElem.value.toLowerCase();
  for (const elem of shareLinksDivElem.children) {
    if (elem.shareData) {
      show = (elem.shareData.label ?? '').toLowerCase().includes(filterText);
      elem.style.display = show ? '' : 'none';
    }
  }
}

function clearShareLinkFilter() {
  shareLinksFilterElem.value = '';
  applyShareLinkFilter();
}

async function updateInfo() { try {
  const response = await fetch(new Request(
    window.location.origin + '/info/' + token,
  ));
  if (response.ok) {
    const json = await response.json();
    nameElem.value = json.name;
    document.title = json.name ? json.name + ' - edit location sharing' : 'Edit location sharing';
    googleShareElem.value = json.google_share_link;
    sharePublic = json.public;
    showPublicSelected();
    let locStr;
    if (json.location) {
      locStr = json.location.lat + ', ' + json.location.lon + ' (accuracy: ' + json.location.acc + ')';
    } else {
      locStr = 'unknown';
    }
    locationElem.textContent = 'Last location: ' + locStr;
    while (shareLinksDivElem.children.length) {
      shareLinksDivElem.removeChild(shareLinksDivElem.children[0]);
    }
    json.shares.forEach(share => {
      const shareElem = shareLinkTemplate.content.children[0].cloneNode(true);
      shareElem.shareData = share;
      const effectiveLabel = share.label ? share.label : '[untitled]';
      // Don't store our own share tokens in addition to our main edit token
      forgetToken(share.token);
      const shareLink = shareLinkForToken(share.token);
      shareElem.querySelector('#share-link-label').textContent = effectiveLabel;
      shareElem.querySelector('#share-link-active').textContent = share.active ? '●' : '○';
      shareElem.querySelector('#share-link-label-div').addEventListener('click', () => {
        onOpenEditShare(share);
        onEditShareActiveSelected(!share.active);
        onCloseEditShare();
      })
      shareElem.querySelector('#share-link-active').classList.toggle('selected', share.active)
      shareElem.querySelector('#share-link-copy').addEventListener('click', () => {
        navigator.clipboard.writeText(shareLink);
        setStatus('Copied!');
      });
      shareElem.querySelector('#share-link-edit').addEventListener('click', () => {
        onOpenEditShare(share);
      });
      shareElem.querySelector('#share-link-delete').addEventListener('click', () => {
        onDeleteConfirm(
          'Delete share link ' + effectiveLabel + ' (' + shareLink + ')?',
          () => {
            setStatus('Deleting...');
            onCloseDeleteConfirm();
            fetch(new Request(
              window.location.origin + '/delete_share/' + token + '/' + share.token,
              {method: 'POST'}
            )).then(() => {
              updateInfo();
              setStatus('Deleted');
            });
          });
      });
      shareLinksDivElem.append(shareElem);
    });
    const hasShareLinks = shareLinksDivElem.children.length > 0;
    shareLinksDivElem.classList.toggle('has-children', hasShareLinks);
    shareLinksFilterElem.parentElement.style.display = hasShareLinks ? 'flex' : 'none';
    applyShareLinkFilter();
  } else {
    throw await response.text();
  }
} catch (err) {
  console.error(err);
  setStatus('Error loading: ' + err);
}}

updateInfo();

function onGpsLoggerUrlCopy() {
  navigator.clipboard.writeText(loggerUrlElem.value);
  setStatus('Copied!');
}

let saveTimeout = null;
async function onSave() { try {
  if (saveTimeout) {
    clearTimeout(saveTimeout);
  }
  saveTimeout = null;
  setStatus('Saving...');
  const expectedStart = 'https://maps.app.goo.gl/';
  if (!googleShareElem.value) {
    googleShareElem.value = '';
  }
  if (googleShareElem.value.startsWith(expectedStart)) {
    googleShareError.style.display = 'none';
  } else {
    googleShareError.style.display = 'block';
    googleShareError.textContent = 'Should start with ' + expectedStart;
    googleShareElem.value = '';
  }
  const response = await fetch(new Request(
    window.location.origin + '/save/' + token, {
      method: 'POST',
      body: JSON.stringify({
        'name': nameElem.value,
        'google_share_link': googleShareElem.value,
        'public': sharePublic,
      }),
    }
  ));
  if (!response.ok) {
    throw await response.text();
  }
  if (currentEditingShare) {
    const response = await fetch(new Request(
      window.location.origin + '/save_share/' + token + '/' + currentEditingShare, {
        method: 'POST',
        body: JSON.stringify({
          'label': editShareLabelElem.value,
          'active': editShareIsActive,
          'timeout': editShareTimeoutElem.value ?
            (parseFloat(editShareTimeoutElem.value) * 60 * 60 + Date.now() / 1000) : null,
        }),
      }
    ));
    if (!response.ok) {
      throw await response.text();
    }
  }
  setStatus('Saved')
  await updateInfo();
} catch (err) {
  console.error(err);
  setStatus('Error saving: ' + err);
}}

function queueSave() {
  if (saveTimeout) {
    clearTimeout(saveTimeout);
  }
  saveTimeout = setTimeout(() => {
    onSave();
  }, 1000);
}

let statusTimeout = null;
function setStatus(text) {
  if (statusTimeout) {
    clearTimeout(statusTimeout);
  }
  statusElem.textContent = text;
  statusTimeout = setTimeout(() => {
    statusTimeout = null;
    statusElem.textContent = '';
  }, 3000);
}

nameElem.addEventListener('input', queueSave);
googleShareElem.addEventListener('input', queueSave);
editShareLabelElem.addEventListener('input', queueSave);
editShareTimeoutElem.addEventListener('input', () => {
  onEditShareActiveSelected(true);
  queueSave();
});

function showPublicSelected() {
  document.getElementById('public-button').classList.toggle('selected', sharePublic);
  document.getElementById('public-description').style.display = sharePublic ? '' : 'none';
  document.getElementById('share-link-public-warning').style.display = sharePublic ? '' : 'none';
  document.getElementById('private-button').classList.toggle('selected', !sharePublic);
  document.getElementById('private-description').style.display = sharePublic ? 'none' : '';
}

function onPublicSelected(newSharePublic) {
  sharePublic = newSharePublic;
  showPublicSelected();
  onSave();
}

async function onShareLinkCreated() {
  setStatus('Creating...');
  await fetch(new Request(
    window.location.origin + '/create_share/' + token,
    {method: 'POST'}
  ));
  await updateInfo();
  setStatus('Created');
}

function onDeleteConfirm(message, callback) {
  deleteConfirmPopupContainer.style.display = 'flex';
  document.getElementById('delete-confirm-message').textContent = message;
  document.getElementById('delete-confirm-confirm').onclick = callback;
}

function onCloseDeleteConfirm() {
  deleteConfirmPopupContainer.style.display = 'none';
}

function onOpenEditShare(share) {
  editSharePopupContainer.style.display = 'flex';
  currentEditingShare = share.token;
  editShareIsActive = share.active;
  showEditShareIsActive();
  const shareLink = shareLinkForToken(share.token);
  document.getElementById('edit-share-link').href = shareLink;
  document.getElementById('edit-share-link').textContent = shareLink;
  editShareLabelElem.value = share.label;
  editShareTimeoutElem.value = share.timeout !== null ? (
    Math.ceil((share.timeout - Date.now() / 1000) / 60.0 / 60.0)) : '';
}

function showEditShareIsActive() {
  document.getElementById('edit-share-active-button').classList.toggle('selected', editShareIsActive);
  document.getElementById('edit-share-inactive-button').classList.toggle('selected', !editShareIsActive);
}

function onEditShareActiveSelected(active) {
  editShareIsActive = active;
  if (!active) {
    editShareTimeoutElem.value = null;
  }
  showEditShareIsActive();
  onSave();
}

async function onCloseEditShare() {
  editSharePopupContainer.style.display = 'none';
  await onSave();
  currentEditingShare = null;
}

function onExpandableToggled(labelElem) {
  labelElem.parentElement.classList.toggle('expanded');
}

function onDeletePin() {
  onDeleteConfirm(
    'Are you sure you want to delete yourself from the map?',
    () => {
      onCloseDeleteConfirm();
      fetch(new Request(
        window.location.origin + '/delete/' + token,
        {method: 'POST'}
      )).then(() => {
        localStorage.setItem('my-token', '');
        window.location.href='/';
      });
    });
}
