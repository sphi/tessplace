let knownTokens = new Set((() => {
  tokens = localStorage.getItem('tokens');
  if (tokens) {
    return JSON.parse(tokens);
  } else {
    return [];
  }
})());
function storeKnownTokens() {
  localStorage.setItem(
    'tokens',
    JSON.stringify(Array.from(knownTokens).filter(token => typeof token === 'string' && token)));
}
function rememberToken(token) {
  if (!knownTokens.has(token)) {
    knownTokens.add(token);
    storeKnownTokens();
  }
}
function forgetToken(token) {
  if (knownTokens.has(token)) {
    knownTokens.delete(token);
    storeKnownTokens();
  }
}
