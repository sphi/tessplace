const map = L.map('map', {zoomControl: false}).setView([0, 0], 4);
let hasSetView = false;
let dynamicMarkers = new Map();
const myToken = localStorage.getItem('my-token');
let viewToken = null;
var myLocation = null;

const markerIcon = L.icon({
  iconUrl: '/assets/marker.svg',
  iconSize: [40, 40], // size of the icon
  iconAnchor: [20, 20], // point of the icon which will correspond to marker's location
});

const selfMarkerIcon = L.icon({
  iconUrl: '/assets/marker.svg',
  iconSize: [40, 40],
  iconAnchor: [20, 20],
  className: 'self-marker',
});

function onCloseErrorPopup() {
  const errorList = document.getElementById('error-list');
  while (errorList.lastElementChild) {
    errorList.removeChild(errorList.lastElementChild);
  }
  document.getElementById('error-popup-container').style.display = 'none';
}

function showError(message) {
  console.error(message);
  if (message.stack) {
    message = String(message) + '\n' + message.stack;
  } else {
    message = String(message);
  }
  document.getElementById('error-popup-container').style.display = 'flex';
  const errorList = document.getElementById('error-list');
  for (const line of message.split('\n')) {
    const p = document.createElement('p');
    p.textContent = line;
    errorList.appendChild(p);
  }
}

try {
  L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
    subdomains: 'abcd',
    maxZoom: 20,
    className: 'map-tiles',
  }).addTo(map);

  if (myToken) {
    rememberToken(myToken);
    const editButtonElem = document.getElementById('edit-button');
    editButtonElem.style.display = 'inline';
    editButtonElem.onclick = () => { window.location.href='/edit/' + myToken; }
  } else {
    const addToMapElem = document.getElementById('add-to-map-button');
    addToMapElem.style.display = 'inline';
  }

  if (window.location.pathname.startsWith('/view/')) {
    viewToken = window.location.pathname.split('/')[2];
    rememberToken(viewToken);
  }
} catch (e) {
  showError(e);
}

function animateLatLng(marker, endPos) {
  const startPos = [marker.getLatLng().lat, marker.getLatLng().lng];
  if (Math.abs(startPos[0] - endPos[0]) + Math.abs(startPos[1] - endPos[1]) < 0.0000001) {
    return;
  }
  const offsetToEnd = [endPos[0] - startPos[0], endPos[1] - startPos[1]];
  const startTime = performance.now() - 16; // Start time is aprox last frame
  const duration  = 0.2;
  const animate = () => {
    const progression = Math.min((performance.now() - startTime) / 1000 / duration, 1);
    if (progression < 1) {
      const delta = 3 * Math.pow(progression, 2) - 2 * Math.pow(progression, 3);
      marker.setLatLng([
        startPos[0] + offsetToEnd[0] * delta,
        startPos[1] + offsetToEnd[1] * delta,
      ]);
      requestAnimationFrame(animate);
    } else {
      marker.setLatLng(endPos);
    }
  };
  animate();
}

function onRequestMyLocation(button) { try {
  if (myLocation == null) {
    button.classList.add("active");

    myLocation = {
      marker: L.marker([0, 0], {icon: selfMarkerIcon}),
      circle: L.circle([0, 0], 1),
      hasCentered: false,
    };

    map.on("locationfound", (e) => {
      if (myLocation) {
        const pos = [e.latlng.lat, e.latlng.lng];
        animateLatLng(myLocation.marker, pos);
        animateLatLng(myLocation.circle, pos);
        myLocation.circle.setRadius(e.accuracy / 2);

        if (!myLocation.hasCentered) {
          myLocation.hasCentered = true;
          map.setView(e.latlng, 15);
          myLocation.marker.addTo(map);
          myLocation.circle.addTo(map);
        }
      }

      if (myToken) {
        fetch(new Request(
          `${window.location.origin}/log/${myToken}?lat=${e.latlng.lat}&lon=${e.latlng.lng}`,
          {method: 'POST'}
        )).catch(e => {
          console.error(e);
        });
      }
    });

    map.locate({ watch: true });
  } else {
    button.classList.remove("active");
    map.stopLocate();
    myLocation.circle.removeFrom(map);
    myLocation.marker.removeFrom(map);
    myLocation = null;
  }
} catch (e) {
  showError(e);
}}

function durationFrom(time) {
  const delta = Date.now() / 1000 - time;
  let span = Math.abs(delta);
  const suffix = delta >= 0 ? ' ago' : ' in the future';
  let units = ' second';
  if (span >= 59) {
    span /= 60;
    units = ' minute';
    if (span >= 59) {
      span /= 60;
      units = ' hour';
      if (span >= 23) {
        span /= 24;
        units = ' day';
      }
    }
  }
  const rounded = Math.round(span);
  return rounded + units + (rounded == 1 ? '' : 's') + suffix;
}

function createTooltip(name, duration) {
  const content = document.createElement('span');
  content.textContent = name;
  const durationSpan = document.createElement('span');
  durationSpan.textContent = ' - ' + duration;
  durationSpan.classList.add('duration');
  content.appendChild(durationSpan);
  return content;
}

function createMarker(location, tooltip, isSelf) {
  return L.marker(location, {icon: isSelf ? selfMarkerIcon : markerIcon})
    .bindTooltip(tooltip, {
      permanent: true,
      direction: 'right',
      offset: [23, 0],
      className: isSelf ? 'self-tooltip' : 'nonself-tooltip',
  });
}

async function updateMarkers() { try {
  try {
    let url = window.location.origin + '/locations';
    if (knownTokens.size) {
      url += '?' + Array.from(knownTokens).map(t => 't=' + t).join('&');
    }
    const response = await fetch(new Request(url)).catch(() => null);
    if (response && response.ok) {
      const result = await response.json();
      for (const token of result.invalid_tokens) {
        forgetToken(token);
      }
      const oldMarkers = dynamicMarkers;
      dynamicMarkers = new Map();
      for (const item of result.locations) {
        const location = [item.lat, item.lon];
        const tooltip = createTooltip(item.name, durationFrom(item.time));
        let marker = oldMarkers.get(item.token);
        if (marker) {
          oldMarkers.delete(item.token)
          marker.setTooltipContent(tooltip);
          animateLatLng(marker, location);
        } else {
          marker = createMarker(location, tooltip, item.token === myToken);
          marker.addTo(map);
        }
        if (!hasSetView) {
          map.setView(location, 15);
          hasSetView = true;
        }
        dynamicMarkers.set(item.token, marker);
      }
      for (let [_token, marker] of oldMarkers) {
        marker.removeFrom(map);
      }
    } else if (response) {
      throw await response.text();
    }
  } catch (e) {
    showError(e);
  }
  setTimeout(updateMarkers, 5000);
} catch (e) {
  showError(e);
}}

updateMarkers()

function onOpenAddToMap() {
  document.getElementById('add-to-map-popup-container').style.display = 'flex';
}

function onCloseAddToMap() {
  document.getElementById('add-to-map-popup-container').style.display = 'none';
}

function onAddToMapNext() { try {
  const name = document.getElementById('name-field').value;
  if (!name) {
    document.getElementById('name-field-error').style.display = 'block';
    return;
  }
  fetch(
    '/create_account?name=' + encodeURIComponent(name),
    {method: 'POST'}
  ).then(resp => {
    if (!resp.ok) {
      throw Error('Failed to create account!');
    }
    return resp.text();
  }).then(text => {
    window.location.href = '/edit/' + text;
  });
} catch (e) {
  showError(e);
}}
